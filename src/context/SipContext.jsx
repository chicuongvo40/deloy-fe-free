// modules
import { createContext, useContext, useRef, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { clvDevice } from 'telesale';
import { useQuery, useMutation } from '@tanstack/react-query';
// Contexts
import { useAuth } from './AuthContext';
import { useAxios } from './AxiosContex';

const Context = createContext();
export function useSip() {
  return useContext(Context);
}

export function SipProvider({ children }) {
  const [rejectStt, setRejectStt] = useState();

  const deviceclv = useRef(null);
  //! config sipjs
  let config = {
    host: 'cf-pbx0001442.cfvn.cloud',
    port: '4433',
    displayName: `109`,
    username: `109`,
    password: `ZvNcLbT01z@`,
    wsServers: 'wss://rtc.cloudfone.vn:4433',
  };
 
  useEffect(() => {
    const device = new clvDevice(config);
    device.on('connecting', () => {
      console.log('connecting!');
    });
    device.on('accepted', () => {
      console.log('We on a phone call!');
      setRejectStt(9);
    });
    device.on('invite', (data) => {
      console.log('invite');
    });
  device.on("accept", (accept) => {
    console.log("accept", accept);
  });
    device.on('cancel', (cancel) => {
      console.log('cancel');
    });
    device.on('rejected', (rejected) => {
      if (rejected.statusCode === 486) {
        setRejectStt(11);
        console.log('khach tu tat may khi chua cam may');
      } else if (rejected.statusCode === 487) {
        setRejectStt(12);
        console.log('user chu dong tat');
      } else if (rejected.statusCode === 500) {
        setRejectStt(13);
        console.log('ko tra loi');
      }
    });
    device.on('destroyed', (data) => {
      console.log('destroyed');
    });
    device.on('failed', (failed) => {
      console.log('failed');
    });
    device.on('bye', () => {
      console.log('bye');
    });
    device?.on('replaced', (replaced) => {
      console.log('replaced');
    });

    device?.on('terminated', (response, cause) => {
      console.log('terminated');
    });
    device?.on('trackAdded', (trackAdded) => {
      console.log('trackAdded');
    });
    device?.on('refer', (response, newSession) => {
      console.log('refer');
    });

    deviceclv.current = device;
  }, []);

  return (
    <Context.Provider
      value={{
        setRejectStt,
        rejectStt,
        deviceclv,
      }}
    >
      {children}
    </Context.Provider>
  );
}
