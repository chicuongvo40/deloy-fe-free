import React from 'react';
import DatePicker from 'react-datepicker';
import { format } from 'date-fns';
import 'react-datepicker/dist/react-datepicker.css';
import styles from './CallReport.module.css';

import { useMutation } from '@tanstack/react-query';
import {
  Box,
  Typography,
  styled,
  Table,
  Button,
} from "@mui/material";
import { useQuery } from '@tanstack/react-query'; // Removed useMutation as it's not used
import vi from 'date-fns/locale/vi';
import { useAxios } from '../../../context/AxiosContex';
import { useState, useEffect } from 'react';
import { formartDate, formatNumber } from '../../../utils/functions';
import { DataGrid } from "@mui/x-data-grid";
import { Loading } from '../../../components/Loading';
import Breadcrumb from "../../../components/Breadcrumb/Breadcrumb";
import SimpleCard from "../../../components/SimpleCardNoButton/SimpleCard";
import DataGridCustomToolbar from "../../../components/DataGridCustomToolbar/DataGridCustomToolbar";
export default function HistoryCall() {
  const [callData, setCallData] = useState([]);
  const [startDate, setStartDate] = useState(
    new Date(new Date().getTime() - 11 * 24 * 60 * 60 * 1000),
  );
  const [endDate, setEndDate] = useState(
    new Date(new Date().getTime() + 1 * 60 * 60 * 1000),
  );

  const { getMissCall } =
  useAxios();
  const handleDateChange = (date) => {
    setSelectedDate(date);
  };
  const postCustomerMutation = useMutation({
    mutationFn: (data) => {
      return getMissCall(data).then((res) => {
        console.log(res);
        setCallData(res.data);
        console.log(callData);
        return res;
      });
    },
  });
  const handleAddUser = (e) => {
    e.preventDefault();
    const data = {
      dateStart: format(startDate.toISOString(),'yyyy-MM-dd HH:mm:ss'),
      dateEnd: format(endDate.toISOString(),'yyyy-MM-dd HH:mm:ss'),
      key: ""
    };
    postCustomerMutation.mutate({
      data
    });
  };

  const columns = [
    {
      field: "stt",
      headerName: "ID",
      flex: 0.5,
    },
    {
      field: "soGoiDen",
      headerName: "Số Điện Thoại",
      flex: 1,
    },
    {
      field: "ngayGoi",
      headerName: "Ngày Gọi",
      flex: 1,
    },
    {
      field: "trangThai",
      headerName: "Trạng Thái",
      flex: 1,
    },

  ];
  const StyledTable = styled(Table)(({ theme }) => ({
    whiteSpace: "pre",
    "& thead": {
      "& tr": { "& th": { paddingLeft: 0, paddingRight: 0 } }
    },
    "& tbody": {
      "& tr": { "& td": { paddingLeft: 0, textTransform: "capitalize" } }
    }
  }));
  const Container = styled("div")(({ theme }) => ({
    margin: "00px",
    [theme.breakpoints.down("sm")]: { margin: "16px" },
    "& .breadcrumb": {
      marginBottom: "30px",
      [theme.breakpoints.down("sm")]: { marginBottom: "16px" }
    }
  }));
  return (
    <>
      <div className={styles.callsboard}>
      <div className={styles.reportCall}>
        <h5 className={styles.title}>Cuộc Gọi Nhỡ</h5>
        <div className={styles.dates}>
          <div>
            <DatePicker
              locale={vi}
              selected={startDate}
              onChange={(date) => {
                console.log(format(date,'yyyy-MM-dd HH:mm:ss'));
                return setStartDate(date);
              }}
              showTimeSelect
              timeFormat="HH:mm"
              timeIntervals={60}
              timeCaption="Time"
              dateFormat="MMMM d, yyyy h:mm"
            />
          </div>
          <strong> Tới</strong>
          <div>
            <DatePicker
              locale={vi}
              selected={endDate}
              onChange={(date) => {
               
                return setEndDate(date);
              }}
              showTimeSelect
              timeFormat="HH:mm"
              timeIntervals={60}
              timeCaption="Time"
              dateFormat="MMMM d, yyyy h:mm"
            />
          </div>


        </div>
        <div className={styles.statistical}>
              <Button     onClick={handleAddUser}>
                Tìm Kiếm
              </Button>
        </div>
      </div>
    </div>
    <Container>
          
          <Box
            width="50vw"
          >
            
              <Box style={{ width: '100%' }}
                 sx={{
                  "& .MuiDataGrid-root": {
                    border: "none",
                  },
                  "& .MuiDataGrid-cell": {
                    borderBottom: "none",
                  },
                  "& .MuiDataGrid-columnHeaders": {
                    backgroundColor: "hsl(209.62,66.95%,53.73%)", // Màu xanh dương
                    color: "black", // Màu trắng
                    borderBottom: "none",
                  },
                  "& .MuiDataGrid-virtualScroller": {
                    backgroundColor: "#FFF", // Màu xanh dương nhạt
                  },
                  "& .MuiDataGrid-footerContainer": {
                    backgroundColor: "#black", // Màu xanh dương
                    color: "#FFF", // Màu trắng
                    borderTop: "none",
                  },
                  "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
                    color: "#2196F3 !important", // Màu đỏ
                  },
                  ".MuiDataGrid-toolbarContainer .MuiText": {
                    color: "#2196F3 !important", /* Màu văn bản */
                  },
                  '& .MuiDataGrid-cell': {
                    fontSize: '12px', // Điều chỉnh kích thước của chữ trong cell
                  },
                  '& .MuiDataGrid-columnHeader': {
                    fontSize: '13px', // Điều chỉnh kích thước của chữ trong header
                  },
                }}
              >
                <DataGrid
                  rows={callData || []}
                  getRowId={(row) => row.id + row.ngayGoi}
                  columns={columns}
                  pageSize={5}
                  autoHeight
                  components={{ Toolbar: DataGridCustomToolbar }}
                  
                />
              </Box>
            
          </Box>
        </Container>
    </>
  );
}
