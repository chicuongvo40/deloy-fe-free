import React from 'react'
import { useQuery } from '@tanstack/react-query';
import { useState, useEffect} from 'react';
import AddIcon from '@mui/icons-material/Add';
import { AiFillFileExcel } from 'react-icons/ai';
import { Button, Typography } from '@mui/material';

//Component
import { CallToUser } from '../../../../components/CallToUser';
import { FormModal } from '../../../../components/FormModal';
import { UserData } from '../../../../components/UserData';
import { ExcelData } from '../../../../components/ExcelData';
//Context
import { useSip } from '../../../../context/SipContext';
import { useAxios } from '../../../../context/AxiosContex';
//hook
import { useExcel } from '../../../../hooks/useExcel';



export default function StaffManager() {
  //Api link
  const { getCustomer } = useAxios();
  //Usestate
  const [customerData, setCustomerData] = useState([]);
  const [idUser, setIdUser] = useState(null);
  const [phone, setPhone] = useState(null);
  const [openFormCall, setOpenFormCall] = useState(false);
  const [openModal, setOpenModal] = useState(false);
  //UsePhone
  const { deviceclv } = useSip();

  // Call to users
  function handleCallToUser(
    number) {
    deviceclv?.current.initiateCall(number);
    setOpenFormCall(true);
  }

  function handleOffCallToUser(
    number) {
    // deviceclv?.current.initiateCall(number);
    setOpenFormCall(false);
  }
  // get customers
  const {
    data: customers,
    isLoading,
    refetch,
  } = 
  useQuery(
    {
      queryKey: ['customers'],
      queryFn: async () => await getCustomer(),
    }
  );

  useEffect(() => {
    if (customers) {
      setCustomerData(customers);
      console.log(customers);
    }
  }, [customers]);
  const [
    handleFile,
    excelFileError,
    excelData,
    setExcelFile,
    setOpenExcel,
    openExcel,
    setExcelData,
  ] = useExcel();
  if (excelFileError) {
    toast(excelFileError, {
      icon: '👏',
      style: {
        borderRadius: '10px',
        background: '#333',
        color: '#fff',
      },
    });
  }
  return (
    <>
       {openExcel ? (
        <ExcelData
        refetch={refetch}
          setExcelData={setExcelData}
          setOpenExcel={setOpenExcel}
          excelData={excelData}
          setExcelFile={setExcelFile}
        />
      ) : null}

      {openModal ? (
        <FormModal
          refetch={refetch}
          setOpenModal={setOpenModal}
        />
      ) : null}

      {openFormCall ? (
        <CallToUser
        handleOffCallToUser={handleOffCallToUser}
          setOpenFormCall={setOpenFormCall}
          idUser={idUser}
          phone={phone}
        />
      ) : null}

      <div >
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <Typography style={{ fontFamily: 'Arial, sans-serif', fontSize: '20px', fontWeight: 'bold', color: '#333' }}>
            Khách Hàng
          </Typography>
          <Button style={{ backgroundColor: '#2196f3', color: '#fff', marginLeft: 'auto' }} startIcon={<AddIcon />} onClick={() => setOpenModal(true)}>
            <Typography >Khách Hàng</Typography>
          </Button>
         
          <Button style={{ backgroundColor: '#2196f3', color: '#fff', marginLeft: '10px', marginRight: '10px' }} startIcon={<AiFillFileExcel />} >
           
            <label style={{ color: 'white' }}
              id='ulbtn'
              htmlFor='upload-photo'
            >Thêm excelss</label>
          </Button>
       
        </div>
        <div >
          <div>
            <input
              className='choosefile'
              onChange={handleFile}
              type='file'
              name='photo'
              id='upload-photo'
            />
          </div>
        </div>
        { customerData ? (<div className=''>
          <UserData
            refetch={refetch}
            handleCallToUser={handleCallToUser}
            isLoading={isLoading}
            groupNamed={customerData}
            setIdUser={setIdUser}
            setPhone={setPhone}
            handleOffCallToUser={handleOffCallToUser}
          />
        </div>) : null}
        
      </div>
    </>
  );
}
