import React from 'react'
import Breadcrumb from "../../../components/Breadcrumb/Breadcrumb";
import SimpleCard from "../../../components/SimpleCard/SimpleCard";
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import {
  Box,
  Icon,
  Table,
  styled,
  TableRow,
  TableBody,
  TableCell,
  TableHead,
  IconButton
} from "@mui/material";
import { red } from '@mui/material/colors';
export default function Customer() {
  const subscribarList = [
    {
      name: "1",
      date: "Huynh Cuong",
     
    },
  ];
  // STYLED COMPONENT
  const StyledTable = styled(Table)(({ theme }) => ({
    whiteSpace: "pre",
    "& thead": {
      "& tr": { "& th": { paddingLeft: 0, paddingRight: 0 } }
    },
    "& tbody": {
      "& tr": { "& td": { paddingLeft: 0, textTransform: "capitalize" } }
    }
  }));
  const Container = styled("div")(({ theme }) => ({
    margin: "00px",
    [theme.breakpoints.down("sm")]: { margin: "16px" },
    "& .breadcrumb": {
      marginBottom: "30px",
      [theme.breakpoints.down("sm")]: { marginBottom: "16px" }
    }
  }));
  return (
    <Container>
      <Box className="breadcrumb">
        <Breadcrumb routeSegments={[{ name: "Admin", path: "/Admin" }, { name: "Quản lý Nhân Viên" }]} />
      </Box>
      <Box
        width="74vw"
      >
        <SimpleCard title="Nhân Viên">
          <StyledTable>
            <TableHead>
              <TableRow>
                <TableCell align="left"><b>STT</b></TableCell>
                <TableCell align="left"><b>Name</b></TableCell>
                <TableCell align="left"><b>Chi Nhánh</b></TableCell>
                <TableCell align="left"><b>Điện Thoại</b></TableCell>
                <TableCell align="left"><b>Vai Trò</b></TableCell>
                <TableCell align="left"><b>Email</b></TableCell>
                <TableCell align="center"><b>Detail</b></TableCell>
                <TableCell align="center"><b>Delete</b></TableCell>
              </TableRow>
            </TableHead>

            <TableBody>
              {subscribarList.map((subscriber, index) => (
                <TableRow key={index}>
                  <TableCell align="left">{subscriber.name}</TableCell>
                  <TableCell align="left">{subscriber.date}</TableCell>
                  <TableCell align="left">{subscriber.date}</TableCell>
                  <TableCell align="left">{subscriber.date}</TableCell>
                  <TableCell align="left">{subscriber.date}</TableCell>
                  <TableCell align="left">{subscriber.date}</TableCell>
                  <TableCell align="center">
                <IconButton>
                  <Icon color="primary"><EditIcon/></Icon>
                </IconButton>
              </TableCell>
              <TableCell align="center">
                <IconButton>
                  <Icon color="error"><DeleteIcon/></Icon>
                </IconButton>
              </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </StyledTable>
        </SimpleCard>
      </Box>

    </Container>

  );
}
