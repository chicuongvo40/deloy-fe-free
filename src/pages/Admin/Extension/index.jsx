import React from 'react'
import Breadcrumb from "../../../components/Breadcrumb/Breadcrumb";
import SimpleCard from "../../../components/SimpleCardNoButton/SimpleCard";
import EditIcon from '@mui/icons-material/Edit';
import {
  Box,
  Icon,
  Table,
  styled,
  TableRow,
  TableBody,
  TableCell,
  TableHead,
  IconButton,
  Typography
} from "@mui/material";
import { useAxios } from '../../../context/AxiosContex';
import { useState, useEffect } from 'react'; 
import { formartDate, formatNumber } from '../../../utils/functions';
import { useQuery } from '@tanstack/react-query';
import { DataGrid } from "@mui/x-data-grid";
import DataGridCustomToolbar from "../../../components/DataGridCustomToolbar/DataGridCustomToolbar";
import { Loading } from '../../../components/Loading';
export default function Extension() {
  const [search, setSearch] = useState("");
  const [searchInput, setSearchInput] = useState("");
  const [callData, setCallData] = useState([]);
  const { getCallHistory } = useAxios();
  const {
    data: callhistory,
    isLoading
  } = useQuery(
    {
      queryKey: ['callhistory'],
      queryFn: () => getCallHistory(),
    }
  );

  useEffect(() => {
    if (callhistory) {
      setCallData(callhistory);
      console.log(callhistory);
    }
  }, [callhistory]);

  const columns = [
    {
      field: "id",
      headerName: "ID",
      flex: 0.5,
    },
    {
      field: "callNumber",
      headerName: "Số Điện Thoại",
      flex: 1,
    },
    {
      field: "dateCall",
      headerName: "Ngày Gọi",
      flex: 1,
      renderCell: (params) => (
        <Typography>{formartDate(params.row.dateCall, 'full')}</Typography>
      )
    },
    {
      field: "direction",
      headerName: "Hướng Gọi",
      flex: 1,
    },

  ];
  const StyledTable = styled(Table)(({ theme }) => ({
    whiteSpace: "pre",
    "& thead": {
      "& tr": { "& th": { paddingLeft: 0, paddingRight: 0 } }
    },
    "& tbody": {
      "& tr": { "& td": { paddingLeft: 0, textTransform: "capitalize" } }
    }
  }));
  const Container = styled("div")(({ theme }) => ({
    margin: "00px",
    [theme.breakpoints.down("sm")]: { margin: "16px" },
    "& .breadcrumb": {
      marginBottom: "30px",
      [theme.breakpoints.down("sm")]: { marginBottom: "16px" }
    }
  }));
  return (
    <>
      {isLoading ? ( // Nếu isLoading là true, hiển thị thành phần Loading
        <div>
          <Loading
            size='90'
            color='#fc3b56'
          />
        </div>
      ) : ( // Ngược lại, hiển thị nội dung đã tải
        <Container>
          <Box className="breadcrumb">
            <Breadcrumb routeSegments={[{ name: "Quản lý Cuộc Gọi" }]} />
          </Box>
          <Box
            width="74vw"
          >
            <SimpleCard title='Lịch Sử Tất Cả Cuộc Gọi'>
              <Box style={{ width: '100%' }}
                 sx={{
                  "& .MuiDataGrid-root": {
                    border: "none",
                  },
                  "& .MuiDataGrid-cell": {
                    borderBottom: "none",
                  },
                  "& .MuiDataGrid-columnHeaders": {
                    backgroundColor: "hsl(209.62,66.95%,53.73%)", // Màu xanh dương
                    color: "black", // Màu trắng
                    borderBottom: "none",
                  },
                  "& .MuiDataGrid-virtualScroller": {
                    backgroundColor: "#FFF", // Màu xanh dương nhạt
                  },
                  "& .MuiDataGrid-footerContainer": {
                    backgroundColor: "#black", // Màu xanh dương
                    color: "#FFF", // Màu trắng
                    borderTop: "none",
                  },
                  "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
                    color: "#2196F3 !important", // Màu đỏ
                  },
                  ".MuiDataGrid-toolbarContainer .MuiText": {
                    color: "#2196F3 !important", /* Màu văn bản */
                  },
                  '& .MuiDataGrid-cell': {
                    fontSize: '12px', // Điều chỉnh kích thước của chữ trong cell
                  },
                  '& .MuiDataGrid-columnHeader': {
                    fontSize: '13px', // Điều chỉnh kích thước của chữ trong header
                  },
                }}//s
              >
                <DataGrid
                getRowId={(row) => row.id}
                  rows={callData || []}
                  columns={columns}
                  pageSize={5}
                  autoHeight
                  components={{ Toolbar: DataGridCustomToolbar }}
                  loading={isLoading}
                />
              </Box>
            </SimpleCard>
          </Box>
        </Container>
      )}
    </>
  );  
}
