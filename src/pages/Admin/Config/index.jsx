import React from 'react'
import Breadcrumb from "../../../components/Breadcrumb/Breadcrumb";
import SimpleCard from "../../../components/SimpleCard/SimpleCard";
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import { useAxios } from '../../../context/AxiosContex';
import { useQuery } from '@tanstack/react-query';
import { useState } from 'react';
import { useEffect } from 'react';
import { AddCampaignAdm } from '../../../components/AdminCpns/AddTicketType';
import { EditCampaignAdm } from '../../../components/AdminCpns/EditTicketType';
import { Loading } from '../../../components/Loading';
import {
  Box,
  Icon,
  Table,
  styled,
  TableRow,
  TableBody,
  TableCell,
  TableHead,
  IconButton
} from "@mui/material";
export default function Level() {
  // STYLED COMPONENT
  const StyledTable = styled(Table)(({ theme }) => ({
    whiteSpace: "pre",
    "& thead": {
      "& tr": { "& th": { paddingLeft: 0, paddingRight: 0 } }
    },
    "& tbody": {
      "& tr": { "& td": { paddingLeft: 0, textTransform: "capitalize" } }
    }
  }));
  const Container = styled("div")(({ theme }) => ({
    margin: "00px",
    [theme.breakpoints.down("sm")]: { margin: "16px" },
    "& .breadcrumb": {
      marginBottom: "30px",
      [theme.breakpoints.down("sm")]: { marginBottom: "16px" }
    }
  }));
  const [brandData, setBrandData] = useState([]);
  const { getTicketType } = useAxios();
  const [openModal, setOpenModal] = useState(false);
  const [editModal, setEditModal] = useState(false);
  const [idBrand, setIdBrand] = useState();
  function handleEdit(id) {
    setIdBrand(id);
    setEditModal(true);
  }
  const {
    data: brand,
    isLoading,
    refetch,
  } = useQuery(
    {
      queryKey: ['brand'],
      queryFn: async () => await getTicketType(),
    }
  );
  useEffect(() => {
    if (brand) {
      setBrandData(brand);
      console.log(brand);
    }
  }, [brand]);
  return (
    <Container>
      {openModal ? (
        <AddCampaignAdm
          refetch={refetch}
          setOpenModal={setOpenModal}
        />
      ) : null}
      {editModal ? (
        <EditCampaignAdm
          refetch={refetch}
          idBrand={idBrand}
          setEditModal={setEditModal}
        />
      ) : null}
      {isLoading ? (
        <div>
          <Loading
            size='90'
            color='#fc3b56'
          />
        </div>
      ) : (
        <>
          <Box className="breadcrumb">
            <Breadcrumb routeSegments={[{ name: "Quản lý loại ticket" }]} />
          </Box>
          <Box
            width="74vw"
          >
            <SimpleCard title="Loại Ticket" setOpenModal={setOpenModal} >
              <StyledTable>
                <TableHead>
                  <TableRow>
                    <TableCell align="left"><b>STT</b></TableCell>
                    <TableCell align="left"><b>Tên loại</b></TableCell>
                    <TableCell align="left"><b>Thời gian dự kiến</b></TableCell>
                    <TableCell align="center"><b>Chỉnh Sửa</b></TableCell>
                    <TableCell align="center"><b>Xóa</b></TableCell>
                  </TableRow>
                </TableHead>
                {brand && brand ?
                  <TableBody>
                    {brand.map((brand, index) => (
                      <TableRow key={index}>
                        <TableCell align="left">{brand.id}</TableCell>
                        <TableCell align="left">{brand.ticketTypeName}</TableCell>
                        <TableCell align="left">{brand.kpiDuration}</TableCell>
                        <TableCell align="center">
                          <IconButton onClick={() => handleEdit(brand.id)}>
                            <Icon color="primary" ><EditIcon /></Icon>
                          </IconButton>
                        </TableCell>
                        <TableCell align="center">
                          <IconButton>
                            <Icon color="error"><DeleteIcon /></Icon>
                          </IconButton>
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody> : <></>
                }

              </StyledTable>
            </SimpleCard>
          </Box>
        </>
      )}


    </Container>

  );
}
