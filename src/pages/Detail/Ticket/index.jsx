

// modules
import { useQuery, useMutation } from '@tanstack/react-query';
import { useParams } from 'react-router-dom';
import { useEffect, useMemo, useState, useRef, useCallback } from 'react';
// context
import { useAxios } from '../../../context/AxiosContex';
import { useAuth } from '../../../context/AuthContext';
//functions
import { formatMoney } from '../../../utils/functions';
import { toast } from 'react-hot-toast';
import { useNavigate } from 'react-router-dom';
import {
  Button,
  Box,
  Grid,
  styled,
  Typography,
  Checkbox,
  Autocomplete
} from "@mui/material";
import CheckBoxOutlineBlankIcon from '@mui/icons-material/CheckBoxOutlineBlank';
import CheckBoxIcon from '@mui/icons-material/CheckBox';
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import { Span } from "../../../components/Typography";
import { format } from 'date-fns';
import img from '../../../assets/customer-service.png'
const TextField = styled(TextValidator)(() => ({
  width: "100%",
  marginBottom: "16px"
}));


const Ticket = () => {
  const {user} = useAuth();
  const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
  const checkedIcon = <CheckBoxIcon fontSize="small" />;
  const navigate = useNavigate();
  const { postTicket, getTicketType, getLevel,postMail } =
    useAxios();
  const { id } = useParams();
  const { idUser } = useParams();
  const { idCon } = useParams();
  const note = useRef();
  const ticketTypeId = useRef();
  const levelId = useRef();
  const {
    data: type,
  } = useQuery(
    {
      queryKey: ['type'],
      queryFn: async () => await getTicketType(),
    }
  );
  const {
    data: level,
  } = useQuery(
    {
      queryKey: ['level'],
      queryFn: async () => await getLevel(),
    }
  );
  //* Mutatuion tạo khách hàng
  const postTicketMutation = useMutation({
    mutationFn: (data) => {
      return postTicket(data).then((res) => {
        return res;
      });
    },
    onSuccess(data) {
      handlePostMail();
      console.log("Da chay goi mail");
      if (data?.status !== 400) {
        toast('Tạo thành công Ticket', {
          icon: '👏',
          style: {
            borderRadius: '10px',
            background: '#333',
            color: '#fff',
          },
        });

      } else {
        toast('Có lỗi trong quá trình tạo Ticker', {
          icon: '👏',
          style: {
            borderRadius: '10px',
            background: '#333',
            color: '#fff',
          },
        });
      }
    },
  });
  //* Hàm tạo Ticket
  const handleAddTicket = (e) => {
    e.preventDefault();
    if (
      !note.current.value ||
      !ticketTypeId.current.value ||
      !levelId.current.value
    ) {
      toast('Không được bỏ trống', {
        icon: '👏',
        style: {
          borderRadius: '10px',
          background: '#333',
          color: '#fff',
        },
      });
      return;
    }
    const data = {
      callId: idCon,
      note: note.current?.value,
      customerId: idUser,
      ticketTypeId: ticketTypeId.current?.value,
      levelId: levelId.current?.value,
      ticketStatusId: 1,
      createdBy: format(Date.now(), 'yyyy-MM-dd')
    };
    postTicketMutation.mutate({
      data
    });
  };
  const top100Films = [
    { title: 'Sự cố'},
    { title: 'Xử Lý' },
    { title: 'Hỏng Hóc' },
    { title: 'Hỗ Trợ' },
  ];
  const postMailMutation = useMutation({
    mutationFn: (data) => {
      return postMail(data).then((res) => {

        console.log(res);
        return res;
      });
    },
  });
  //* Hàm tạo Ticket
  const handlePostMail = (e) => {
   
  const data = {
    subject: "Tạo Ticket Thành Công",
    body: "Chúng tôi đã gửi yêu cầu của bạn đi xử lý. Chúng tôi xe gửi mail để cập nhật tiến độ",
    toAddresses:['cuonghcse150679@fpt.edu.vn']
  };
  postMailMutation.mutate({
    data
  });
};
  return (
    <Box
      width="100%"
      justifyContent="center"
      alignItems="center"
      sx={{
        backgroundColor: 'white',
        '& > *': {
          margin: '10px', // Apply margin of 10 units to direct children
        },
      }}
    >
      <Box>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <Typography style={{ fontFamily: 'Arial, sans-serif', fontSize: '20px', fontWeight: 'bold', color: '#333' }}>
            Tạo Ticket
          </Typography>
        </div>
      </Box>
      <ValidatorForm onSubmit={handleAddTicket} onError={() => null}>
        <Grid container spacing={6}>
          <Grid item lg={4} md={4} sm={12} xs={12} sx={{ mt: 2 }}>
            <TextField
              type="text"
              name="Call Code"
              id="standard-basic"
              label="Call Code"
              value={idCon}
            />

            <TextField
              type="text"
              name="Customer"
              label="Customer"
              value={idUser}
            />
            <TextField
              type="text"
              name="Type"
              label="Note"
              multiline
              rows={5}
              inputRef={note}
              fullWidth  // Mở rộng ô text để lấp đầy toàn bộ chiều rộng của parent
            />

          </Grid>

          <Grid item lg={4} md={4} sm={12} xs={12} sx={{ mt: 2 }}>
            {/* <TextField
              type="text"
              name="Level"
              id="standard-basic"
              inputRef={levelId}
              label="Level"
           
            /> */}
            <Box>
              <Typography sx={{ fontSize: 15 }}>Chọn Cấp Độ</Typography>
              <div>
                {/* onChange={(e) => setsourceId(e.target.value)} value={xsourceId} */}
                <select style={{ width: '100%', height: '4.5vh' }} ref={levelId}>
                  {level &&
                    level.map((lv, index) => {
                      if (lv?.levelName) {
                        return (
                          <option
                            key={index}
                            value={lv.id}
                          >
                            {lv.levelName}
                          </option>
                        );
                      }
                    })}
                </select>
              </div>
            </Box>

            <Box sx={{ marginTop: 2 }}>

              <Typography sx={{ fontSize: 15 }}>Chọn Loại</Typography>
              <div>
                {/* onChange={(e) => setsourceId(e.target.value)} value={xsourceId} */}
                <select style={{ width: '100%', height: '4.5vh' }} ref={ticketTypeId}>
                  {type &&
                    type.map((lv, index) => {
                      if (lv?.ticketTypeName) {
                        return (
                          <option
                            key={index}
                            value={lv.id}
                          >
                            {lv.ticketTypeName}
                          </option>
                        );
                      }
                    })}
                </select>
              </div>
            </Box>

            <Box style={{ marginTop: 25 }}>
              <Autocomplete
                multiple
                id="checkboxes-tags-demo"
                options={top100Films}
                disableCloseOnSelect
                getOptionLabel={(option) => option.title}
                renderOption={(props, option, { selected }) => (
                  <li {...props}>
                    <Checkbox
                      icon={icon}
                      checkedIcon={checkedIcon}
                      style={{ marginRight: 8 }}
                      checked={selected}
                    />
                    {option.title}
                  </li>
                )}
                style={{ width: 275 }}
                renderInput={(params) => (
                  <TextField {...params} label="Tags" placeholder="Favorites" />
                )}
              />
            </Box>
          </Grid>
          <Grid item lg={4} md={4} sm={12} xs={12} sx={{ mt: 2 }}>
          <Box
      sx={{
        width: '100%', // 100% chiều rộng của viewport
        display: 'block',
      }}
    >
      <img src={img} alt="Description of the image" style={{ maxHeight: '100px', maxWidth: '10vw' }} />
      <div className="c" style={{ fontSize: "16px" }}>Tên Nhân Viên: Cường</div>
<div className="c" style={{ fontSize: "16px" }}>Vai Trò: Nhân Viên</div>
<div className="c" style={{ fontSize: "16px" }}>Chúc Vụ: Tổng Đài</div>

    </Box>
          </Grid>

        </Grid>
        <Button color="primary" variant="contained" type="submit">
          <Span sx={{ pl: 1, textTransform: "capitalize" }}>Submit</Span>
        </Button>
      </ValidatorForm>
    </Box>
  );
};

export default Ticket;
