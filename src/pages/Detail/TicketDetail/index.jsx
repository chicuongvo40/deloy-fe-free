import React from 'react'
import { useQuery } from '@tanstack/react-query';
import { useState, useEffect } from 'react';
import AddIcon from '@mui/icons-material/Add';
import { AiFillFileExcel } from 'react-icons/ai';
import {
  Box,
  Icon,
  Paper, Grid, Divider,
  IconButton,
  Typography,
} from "@mui/material";
import { useParams } from 'react-router-dom';
import { Edit as EditIcon, PriorityHigh as PriorityHighIcon } from '@mui/icons-material';
import { useRef, useCallback } from 'react';
import { formartDate, formatNumber } from '../../../utils/functions';
import { FcCallback } from 'react-icons/fc';
import { CgDetailsMore } from 'react-icons/cg';
import DataGridCustomToolbar from "../../../components/DataGridCustomToolbar/DataGridCustomToolbar";
import { DataGrid } from "@mui/x-data-grid";
import { useAxios } from '../../../context/AxiosContex';
import { TiDelete } from 'react-icons/ti';
import AddCampaignAdm from '../../../components/AdminCpns/AddStaff';
import './TicketDetail.css';
export default function StaffManager() {

  const { getTicketById,getCustomerById,getLogByTicketId } = useAxios();
  const [customerData, setCustomerData] = useState(null);
  const [customerDatas, setCustomerDatas] = useState(null);
  const [log, setLog] = useState(null);
  const { id } = useParams();
  const { idUser } = useParams();
  const {
    data: customers,
    isLoading,

  } = 
  useQuery(
    {
      queryKey: ['customers',id],
      queryFn: async () => await getTicketById(id),
    }
  );

  const {
    data: customer,
  } = 
  useQuery(
    {
      queryKey: ['customer',idUser],
      queryFn: async () => await getCustomerById(idUser),
    }
  );
  useEffect(() => {
    if (customer) {
      setCustomerDatas(customer);
      console.log(customer);
    }
  }, [customer]);
  useEffect(() => {
    if (customers) {
      setCustomerData(customers);
      console.log(customers);
    }
  }, [customers]);

  function handleOnClick(id){
    console.log('Co Nhan nha');
    setEditOpenModal(true);
    setId(id);
  }

  const {
    data: logticket,
  } = 
  useQuery(
    {
      queryKey: ['logticket',id],
      queryFn: async () => await getLogByTicketId(id),
    }
  );
  useEffect(() => {
    if (logticket) {
      setLog(logticket);
      console.log(logticket);
    }
  }, [logticket]);
  return (
    <>
    {!isLoading ? (
      <Box>
        <Box mb='10px'>
  <Grid container spacing={2}>
    <Grid item xs={6}>
    {customers ?
      (<Paper elevation={3} style={{ padding: '20px', height:"420px" }}>
          <Grid container spacing={2} alignItems="center">
            <Grid item xs={12}>
              <Typography variant="h5" gutterBottom>
                Ticket Detail
                <IconButton color="primary" aria-label="edit ticket">
                  <EditIcon />
                </IconButton>
              </Typography>
              <Divider style={{ marginBottom: '10px' }} />
            </Grid>
            <Grid item xs={12}>
              <Typography variant="subtitle1">Tag</Typography>
              <Typography>#Tag #Tag #Tag</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant="subtitle1">ID:</Typography>
              <Typography>{customers.id}</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant="subtitle1">Title:</Typography>
              <Typography>Tiêu đề</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant="subtitle1">Description:</Typography>
              <Typography>{customers.note}</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant="subtitle1">Ngày Tạo:</Typography>
              <Typography>{customers.createdBy}</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant="subtitle1">Status:</Typography>
              <Typography>{customers.ticketStatusId}</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant="subtitle1">Priority:</Typography>
              <Typography>
                <PriorityHighIcon style={{ color: 'red' }} />
                {customers.levelId}
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant="subtitle1">Assignee:</Typography>
              <Typography>Huỳnh Chí Cường</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant="subtitle1">Type</Typography>
              <Typography>{customers.ticketTypeId}</Typography>
            </Grid>
          </Grid>
        </Paper>) : null
}
    </Grid>
    <Grid item xs={6}>
    {customerDatas ? (
  <>
    <Paper elevation={3} style={{ padding: '20px', height: '290px' }}>
      <Grid container spacing={2} alignItems="center">
        <Grid item xs={12}>
          <Typography variant="h5" gutterBottom>
            Khách Hàng
            <IconButton color="primary" aria-label="edit ticket">
              <EditIcon />
            </IconButton>
          </Typography>
          <Divider style={{ marginBottom: '10px' }} />
        </Grid>
        <Grid item xs={6}>
          <Typography variant="subtitle1">ID:</Typography>
          <Typography>{customerDatas.id}</Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography variant="subtitle1">Số Điện Thoại</Typography>
          <Typography>{customerDatas.phoneNumber}</Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography variant="subtitle1">Địa Chỉ</Typography>
          <Typography>{customerDatas.address}</Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography variant="subtitle1">Ngày Sinh</Typography>
          <Typography>{customerDatas.dayOfBirth}</Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography variant="subtitle1">Họ và Tên</Typography>
          <Typography>{customerDatas.name}</Typography>
        </Grid>
      </Grid>
    </Paper>
    <Typography>Log của Ticket: </Typography>
    {logticket ? (
      <Box>
        {logticket.map((item, index) => (
          <div key={index}>
            <Typography variant="subtitle1">{item.note} -- {item.timeStart}</Typography>
            {/* Render other properties similarly */}
          </div>
        ))}
      </Box>
    ) : null}
  </>
) : null}


    </Grid>
  </Grid>
</Box>

        

      </Box>
    ) : null}
    </>
    

  );
}
