import styles from './CallReport.module.css';
import DatePicker from 'react-datepicker';
import vi from 'date-fns/locale/vi';
import 'react-datepicker/dist/react-datepicker.css';
import { useState } from 'react';
import ChartCircleCall from '../../../components/Charts/ChartCircleCall1';
import { useQuery } from '@tanstack/react-query';
import { useAxios } from '../../../context/AxiosContex';
import { toast } from 'react-hot-toast';
export default function ManagerTicket() {
  const [startDate, setStartDate] = useState(
    new Date(new Date().getTime() - 30 * 24 * 60 * 60 * 1000),
  );
  const [endDate, setEndDate] = useState(
    new Date(new Date().getTime() + 24 * 60 * 60 * 1000),
  );
  
  const { getReportTicket } = useAxios();
  const [callData, setCallData] = useState([]);
  const { isLoading, error, data: ticket } = useQuery({
    queryKey: ['ticket', startDate, endDate], // Corrected queryKey
    queryFn: async () => {
      return await getReportTicket({
        startDate: startDate.toISOString(),
        endDate: endDate.toISOString(),
      });
    },
  });
  // useEffect(() => {
  //   if (htrCall) {
  //     setCallData(htrCall);
  //     console.log(htrCall);
  //   }
  //   else{
  //     console.log('sao có giá trị mà ko in');
  //   }
  // }, [htrCall]);
  return (
    <>
     <div className={styles.callsboard}>
      <div className={styles.reportCall}>
        <h5 className={styles.title}>Báo cáo ngày</h5>
        <div className={styles.dates}>
          <div>
            <DatePicker
              locale={vi}
              selected={startDate}
              onChange={(date) => {
                if (endDate - date > 31 * 24 * 60 * 60 * 1000) {
                  toast('Chỉ xử lý được trong khoảng 30 ngày', {
                    icon: '👏',
                    style: {
                      borderRadius: '10px',
                      background: '#333',
                      color: '#fff',
                    },
                  });
                  return;
                }
                return setStartDate(date);
              }}
            />
          </div>
          <strong> Tới</strong>
          <div>
            <DatePicker
              locale={vi}
              selected={endDate}
              onChange={(date) => {
                if (date - startDate > 31 * 24 * 60 * 60 * 1000) {
                  toast('Chỉ xử lý được trong khoảng 30 ngày', {
                    icon: '👏',
                    style: {
                      borderRadius: '10px',
                      background: '#333',
                      color: '#fff',
                    },
                  });
                  return;
                }
                return setEndDate(date);
              }}
            />
          </div>
        </div>
        <div className={styles.statistical}>
        <h4>Thống kê theo ngày: {startDate.toISOString().split('T')[0]} Tới ngày {endDate.toISOString().split('T')[0]}</h4>
          <div className={styles.charts}>
            <div className={styles.chart}>
              <h5>Thống kê ticket</h5>
              <div>
                <ChartCircleCall htrCall={ticket} />
              </div>
            </div>
            <div className={styles.listCall}>
              <div className={styles.box__content}>
                <div className={styles.head__box}>
                  <h3>Ticket chi tiết theo ngày</h3>
                </div>
                <div className={styles.head_new}>
                  <span>STT</span>
                  <span>Ngày</span>
                  <span>Tiếp Nhận</span>
                  <span>Chờ duyệt</span>
                  <span>Hoàn Thành</span>
                  <span>Kết Thúc</span>
                  <span>Tổng</span>
                </div>
                
                {ticket?.listCall &&
                  ticket?.listCall
                    .sort(function (a, b) {
                      return (
                        new Date(b.date).getTime() - new Date(a.date).getTime()
                      );
                    })
                    .map((htr, index) => (
                      <div
                        key={index}
                        className={styles.content_new}
                      >
                        <span>{index + 1}</span>
                        <span>{htr.date}</span>
                        <span className={`${htr.aa != 0 && styles.bold}`}>
                          {htr.aa}
                        </span>
                     
                        <span className={`${htr.bb != 0 && styles.bold3}`}>
                          {htr.bb}
                        </span>
                        <span
                          className={`${htr.cc != 0 && styles.bold4}`}
                        >
                          {htr.cc}
                        </span>
                        <span
                          className={`${htr.dd != 0 && styles.bold4}`}
                        >
                          {htr.dd}
                        </span>
                        <span>
                          {htr.aa +
                            htr.bb +
                            htr.cc+
                            htr.dd}
                        </span>
                      </div>
                    ))}

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </>
  );
}