import styles from './CallReport.module.css';
import {
  Box,
  Typography,
  styled,
  Table,
} from "@mui/material";
import DatePicker from 'react-datepicker';
import vi from 'date-fns/locale/vi';
import { AiFillEdit } from 'react-icons/ai';
import { GrView } from 'react-icons/gr';
import 'react-datepicker/dist/react-datepicker.css';
import { useState } from 'react';
import { useEffect } from 'react';
import { FcCallback } from 'react-icons/fc';
import { Loading } from '../../../components/Loading';
import ChartCircleCall from '../../../components/Charts/ChartCircleCall';
import { useQuery } from '@tanstack/react-query';
import { useAxios } from '../../../context/AxiosContex';
import { useAuth } from '../../../context/AuthContext';
import { toast } from 'react-hot-toast';
import { AiOutlinePlus, AiFillFileExcel } from 'react-icons/ai';
import { useNavigate } from 'react-router-dom';
export default function CustomerReport() {
  const [startDate, setStartDate] = useState(
    new Date(new Date().getTime() - 30 * 24 * 60 * 60 * 1000),
  );
  const [endDate, setEndDate] = useState(
    new Date(new Date().getTime() + 24 * 60 * 60 * 1000),
  );
  const [xstaff, setStaff] = useState([]);
  const [selectMonth, setSelectMonth] = useState(new Date());
  // const navigate = useNavigate();
  const { getStaff } =
    useAxios();
  const {
    data: staff,
    isLoading
  } = useQuery(
    {
      queryKey: ['staff'],
      queryFn: () => getStaff(),
    }
  );
  useEffect(() => {
    if (staff) {
      setStaff(staff);
      console.log(staff);
    }
  }, [staff]);
  return (
    <>
      <div className={styles.head__box}>
        <h3>Quản lý nhân viên</h3>
      </div>

      <div className={styles.listCall}>
        {isLoading ? (
          <div className={styles.loading__tb}>
            <Loading
              size='90'
              color='#fc3b56'
            />
          </div>
        ) : (
          <>
            <div className={styles.head_new}>
              <span>STT</span>
              <span>Tên nhân viên</span>
              <span>Máy nhánh</span>
              <span>Trạng thái</span>
              <span>Số điện thoại</span>
              <span>Địa chỉ</span>
              <span>Sửa</span>
              <span>Xem</span>
              <span>Gọi qua máy nhánh</span>
            </div>
            {xstaff &&
              xstaff.map((elm, index) => (
                <div
                  key={index}
                  className={styles.content_new}
                >
                  <span>{elm.id}</span>
                  <span>{elm.firstName + ' ' + elm.lastName}</span>
                  <span>{elm.userName}</span>
                  <span>
                    {elm.status == 1 && 'Offline'}
                    {elm.status == 2 && 'Offline'}
                    {elm.status == 3 && 'Bận'}
                  </span>
                  <span>{elm.password}</span>
                  <span>{elm.address}</span>
                  <span>
                    <AiFillEdit
                      // onClick={() => handleEdit(elm.id)}
                      className={styles.editBtn}
                      style={{
                        cursor: 'pointer',
                        color: '#1775f1',
                        fontSize: '22px',
                      }}
                    />
                  </span>
                  <span>
                    <GrView
                      className={styles.editBtn}
                      // onClick={() => navigate(`/details-staff/${elm.id}`)}
                      style={{
                        cursor: 'pointer',
                        color: '#1775f1',
                        fontSize: '22px',
                      }}
                    />
                  </span>
                  <span>
                    <FcCallback
                      // onClick={() => handleCallToUser(elm.phoneNumber)}
                      className={styles.editBtn}
                      style={{
                        cursor: 'pointer',
                        color: '#1775f1',
                        fontSize: '22px',
                      }}
                    />
                  </span>
                </div>
              ))}
          </>
        )}
      </div>
    </>
  );
}