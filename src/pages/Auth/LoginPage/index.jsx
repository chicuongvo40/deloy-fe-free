import React from 'react'
import './Login.css'
import { FaLock, FaUser } from 'react-icons/fa';
import { toast } from 'react-hot-toast';
import { useRef } from 'react';
import { useAuth } from '../../../context/AuthContext';
const Login = () => {
  const nameRef = useRef();
  const passwordRef = useRef();
  const { login, message } = useAuth();
  if (message) {
    toast('Tài khoản không đúng', {
      icon: '👏',
      style: {
        borderRadius: '10px',
        background: '#333',
        color: '#fff',
      },
    });
  }

  async function handleLogin(e) {
    e.preventDefault();
    console.log('Tao có nhấn nha');
    const name = nameRef.current?.value;
    const password = passwordRef.current?.value;
    if (!name || !password) {
      return toast('Không được bỏ trống', {
        icon: '👏',
        style: {
          borderRadius: '10px',
          background: '#333',
          color: '#fff',
        },
      });
    }
    await login.mutateAsync({ email: name, password });
  }

  return (
    <div className='login'>

      <div className='wrapper'>

        <form>

          <h1>Login</h1>

          <div className='input-box'>

            <input type='text' placeholder='Username' required  ref={nameRef}/>

            <FaUser className='icon' />
          
          </div>

          <div className='input-box'>

            <input type='password' placeholder='Password' required  ref={passwordRef}/>

            <FaLock className='icon' />

          </div>

          <div className='remember-forgot'>

            <label><input type='checkbox' />Remember me</label>

            <a href='a'>Forgot password</a>

          </div>

          <button onClick={handleLogin}>Login</button>

          <div className='register-link'>

            <p>Don't have an account ? <a href='a' />Register</p>

          </div>
         

        </form>

      </div>

    </div>
  )
}

export default Login;