import React from 'react'
import './sidebar.css'

//Thêm hình
import logo from '../../assets/logo-color.png'

//Thêm Icon
import { IoMdSpeedometer } from 'react-icons/io'
import { GrSchedules } from 'react-icons/gr'
import { CiHeadphones } from 'react-icons/ci'
import { MdHistoryToggleOff } from 'react-icons/md'
import { BiTask } from 'react-icons/bi'
import { useNavigate, useLocation } from 'react-router-dom';
const SideBar = () => {
    const navigate = useNavigate();

    const location = useLocation();
    return (
        <div className='sideBar grid'>
            <div className="logoDiv flex">
                <img  src={logo} alt='Image Name' />
            </div>

            <div className="menuDiv">
                {/* <h3 className='divTittle'>
                    QUẢN LÝ
                </h3> */}
                <ul className='menuLists grid'>
                <li className={'listItem ' + 
                        (location.pathname === '/admin' ? 'active' : '')
                    }
                        onClick={() => navigate('/admin')}>
                        <a className='menuLink flex' >
                            <CiHeadphones className='icon' />
                            <span className="smallText">
                                Quản lý chi nhánh
                            </span>
                        </a>
                    </li>
                    
                    <li className={'listItem ' + 
                        (location.pathname === '/admin/source' ? 'active' : '')
                    } onClick={() => navigate('/admin/source')}>
                        <a className='menuLink flex' >
                            <BiTask className='icon' />
                            <span className="smallText">
                                Quản lý kênh KH
                            </span>
                        </a>
                    </li>
               
                     <li className={'listItem ' + 
                        (location.pathname === '/admin/customerlevel' ? 'active' : '')
                    } onClick={() => navigate('/admin/customerlevel')}>

                        <a  className='menuLink flex' >
                            <GrSchedules className='icon' />
                            <span className="smallText">
                                Cấp Độ Khách Hàng
                            </span>
                        </a>
                    </li>

                    
                    <li className={'listItem ' + 
                        (location.pathname === '/admin/tickettag' ? 'active' : '')
                    } onClick={() => navigate('/admin/tickettag')}>
                        <a className='menuLink flex' >
                            <MdHistoryToggleOff className='icon' />
                            <span className="smallText">
                                Ticket Tag
                            </span>
                        </a>
                    </li>

                  
                    <li className={'listItem ' + 
                        (location.pathname === '/admin/level' ? 'active' : '')
                    } onClick={() => navigate('/admin/level')}>
                        <a className='menuLink flex' >
                            <MdHistoryToggleOff className='icon' />
                            <span className="smallText">
                                Cấp Độ Ticket
                            </span>
                        </a>
                    </li>

                    <li className={'listItem ' + 
                        (location.pathname === '/admin/configuration' ? 'active' : '')
                    } onClick={() => navigate('/admin/configuration')}>
                        <a className='menuLink flex' >
                            <GrSchedules className='icon' />
                            <span className="smallText">
                                Quản Lý loại Ticket
                            </span>
                        </a>
                    </li>

                 
                    <li className={'listItem ' + 
                        (location.pathname === '/admin/extension' ? 'active' : '')
                    } onClick={() => navigate('/admin/extension')}>
                    
                        <a className='menuLink flex' >
                            <GrSchedules className='icon' />
                            <span className="smallText">
                                Quản lý cuộc gọi
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
            <div className="sideBarCard">
                <IoMdSpeedometer className='icon' />
                <div className="cardContent">
                    <div className="circle1"></div>
                    <div className="circle1"></div>
                    <h3>Trung tâm trợ giúp</h3>
                    <p>Chúc bạn có ngày làm việc vui vẻ.</p>
                    <button className='btn'>Xin Chào</button>

                </div>
            </div>
        </div>
    )
}

export default SideBar