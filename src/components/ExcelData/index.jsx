import styles from './ExcelData.module.css';
import { Button } from '../Button';
import { toast } from 'react-hot-toast';
import v2 from '../../assets/v2.png';
import { useQuery, useMutation } from '@tanstack/react-query';
import { useAuth } from '../../context/AuthContext';
import { useAxios } from '../../context/AxiosContex';
import { ExcelDateToJSDate, formatNumber } from '../../utils/functions';
import { useRef } from 'react';

export function ExcelData({refetch, setOpenExcel, excelData, setExcelFile }) {
  //* Link api
  const { getSource,  getLevelUser,  postCustomer } =
    useAxios();

  if (!excelData.length) {
    setExcelFile(null);
    toast('Không có dữ liệu', {
      icon: '👏',
      style: {
        borderRadius: '10px',
        background: '#333',
        color: '#fff',
      },
    });
    return;
  }
  const levelRef = useRef();
  const channelRef = useRef();
  const sourceRef = useRef();
  const capaignRef = useRef();
  //* Mutatuion tạo khách hàng
  const postCustomerMutation = useMutation({
    mutationFn: (data) => {
      return postCustomer(data).then((res) => {
        refetch();
        console.log(data);
        return res;
      });
    },
    onSuccess(data) {
      if (data.status == 'Fail') {
        toast('Quá trình thêm bị lỗi', {
          icon: '👏',
          style: {
            borderRadius: '10px',
            background: '#333',
            color: '#fff',
          },
        });
        return;
      }
    },
  });
  function handleAddAll() {
    const requests = [];
    for (const field of excelData) {
      const data = {
      customerLevelId: field.customerLevelId,
      sourceId: field.sourceId,
      branchId: field.branchId,
      lastName: field.lastName,
      firstName: field.firstName,
      phoneNumber: field.phoneNumber,
      status: field.status,
      gender: field.gender,
      address: field.address,
      dayOfBirth: field.dayOfBirth,
      dateCreated:  field.dateCreated,
      lastEditedTime: field.lastEditedTime,
      name: field.name
      };
      requests.push({
        data
      });
    }
    Promise.allSettled(
      requests.map(async (us) => {
        try {
          await postCustomerMutation.mutate(us);
        } catch (err) {
          console.error(`I'm down, this time. ${err}`);
        }
      }),
    ).then(() => {
      refetch();
      setExcelFile(null);
      setOpenExcel(false);
      toast('Thêm thành công', {
        icon: '👏',
        style: {
          borderRadius: '10px',
          background: '#333',
          color: '#fff',
        },
      });
      return;
    });
  }
  const fields = [
    'customerLevelId',
    'sourceId',
    'branchId',
    'lastName',
    'firstName',
    'phoneNumber',
    'status',
    'gender',
    'address',
    'dayOfBirth',
    'dateCreated',
   ' lastEditedTime',
    'name'
  ];
  let content;
  Object.keys(excelData[0]).map((elm) => {
    if (!fields.includes(elm)) {
      content = (
        <>
          <div className={styles.excelData}>
            <div className={styles.contentExcel}>
              <h5>Các trường không hợp lệ</h5>
              <span>Hãy nhập cột theo mẫu sau</span>
              <img
                src={v2}
                alt='v2'
              />
              <div className={styles.buttons}>
                <Button
                  className={styles.cancelbtn}
                  onClick={(e) => {
                    e.preventDefault();
                    setExcelFile(null);
                    setOpenExcel(false);
                  }}
                >
                  Thoát
                </Button>
              </div>
            </div>
          </div>
        </>
      );
      return;
    }
    content = (
      <div className={styles.excelData}>
        <div className={styles.contentExcel}>
          <h5>
            Tất cả dữ liệu từ excel{' '}
            <span>
              ( Vui lòng đẩy lên dữ liệu )
              <strong style={{ display: 'block', marginTop: '15px' }}>
                Lưu ý không được bỏ trống mục nào để tránh lỗi xảy ra khi upload
              </strong>
            </span>
          </h5>
          <div className={styles.data}>
            <table>
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Phone</th>
                </tr>
              </thead>
              <tbody>
                {excelData &&
                  excelData.map((elm, index) => (
                    <tr key={index}>
                      <td>{elm.name}</td>
                      <td>{elm.phoneNumber}</td>
                    </tr>
                  ))}
              </tbody>
            </table>
          </div>
          <div className={styles.buttons}>
            <Button
              className={styles.cancelbtn}
              onClick={(e) => {
                e.preventDefault();
                setExcelFile(null);
                setOpenExcel(false);
              }}
            >
              Hủy
            </Button>
            <Button
              onClick={handleAddAll}
              className={styles.cancelbtn2}
            >
              Thêm tất cả
            </Button>
          </div>
        </div>
      </div>
    );
  });
  return content;
}
