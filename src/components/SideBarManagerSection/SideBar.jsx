import React from 'react'
import './sidebar.css'
//Thêm hình
import logo from '../../assets/tele.jpg'
//Thêm Icon
import { IoMdSpeedometer } from 'react-icons/io'
import { GrSchedules } from 'react-icons/gr'
import { CiHeadphones } from 'react-icons/ci'
import { MdHistoryToggleOff } from 'react-icons/md'
import { BiTask } from 'react-icons/bi'
import { useNavigate, useLocation } from 'react-router-dom';
const SideBarManagerSection = () => {
    const navigate = useNavigate();

    const location = useLocation();
    return (
        <div className='sideBarmanager grid'>
            <div className="logoDiv flex">
                <img src={logo} alt='Image Name' />
            </div>

            <div className="menuDiv">
                <h3 className='divTittle'>
                    Báo Cáo
                </h3>
                

                <ul className='menuLists grid'>
                <li className={'listItem ' +
                        (location.pathname === '/manager/staff' ? 'active' : '')
                    }
                        onClick={() => navigate('/manager/staff')}>
                        <a className='menuLink flex' >
                            <MdHistoryToggleOff className='icon' />
                            <span className="smallText">
                                Báo Nhân Viên
                            </span>
                        </a>
                    </li>

                    <li className={'listItem ' +
                        (location.pathname === '/manager' ? 'active' : '')
                    }
                        onClick={() => navigate('/manager')}>
                        <a className='menuLink flex' >
                            <CiHeadphones className='icon' />
                            <span className="smallText">
                                Báo Cáo Cuộc Gọi
                            </span>
                        </a>
                    </li>

                

                    <li className={'listItem ' +
                        (location.pathname === '/manager/ticket' ? 'active' : '')
                    }
                        onClick={() => navigate('/manager/ticket')}>
                        <a className='menuLink flex' >
                            <MdHistoryToggleOff className='icon' />
                            <span className="smallText">
                                Báo Ticket
                            </span>
                        </a>
                    </li>
                    <li className={'listItem ' +
                        (location.pathname === '/manager/tickets' ? 'active' : '')
                    }
                        onClick={() => navigate('/manager/tickets')}>
                        <a className='menuLink flex' >
                            <MdHistoryToggleOff className='icon' />
                            <span className="smallText">
                                Tiếp Nhận Ticket
                            </span>
                        </a>
                    </li>
                    <li className={'listItem ' +
                        (location.pathname === '/manager/ticket/aaaaaaa' ? 'active' : '')
                    }
                        onClick={() => navigate('/manager/ticket')}>
                        <a className='menuLink flex' >
                            <MdHistoryToggleOff className='icon' />
                            <span className="smallText">
                                KPI Ticket
                            </span>
                        </a>
                    </li>
                    
                </ul>
                <div style={{ marginTop: '3vh' }}></div>
                

                <h3 className='divTittles'>
                    Quản Lý
                </h3>
                <ul className='menuLists grid'>
                    <li className={'listItem ' +
                        (location.pathname === '/manager/m' ? 'active' : '')
                    }
                        onClick={() => navigate('/manager/m')}>
                        <a className='menuLink flex' >
                            <CiHeadphones className='icon' />
                            <span className="smallText">
                                Khách Hàng
                            </span>
                        </a>
                    </li>


                    <li className={'listItem ' +
                        (location.pathname === '/manager/mcallhistory' ? 'active' : '')
                    }
                        onClick={() => navigate('/manager/mcallhistory')}>
                        <a className='menuLink flex' >
                            <MdHistoryToggleOff className='icon' />
                            <span className="smallText">
                                Lịch Sử Gọi
                            </span>
                        </a>
                    </li>




                    <li className={'listItem ' +
                        (location.pathname === '/manager/mschudule' ? 'active' : '')
                    }
                        onClick={() => navigate('/manager/mschudule')}>
                        <a className='menuLink flex' >
                            <GrSchedules className='icon' />
                            <span className="smallText">
                                Đặt Lịch
                            </span>
                        </a>
                    </li>

                    <li className={'listItem ' +
                        (location.pathname === '/manager/mmisscall' ? 'active' : '')
                    }
                        onClick={() => navigate('/manager/mmisscall')}>
                        <a className='menuLink flex' >
                            <GrSchedules className='icon' />
                            <span className="smallText">
                                Cuộc Gọi Nhỡ
                            </span>
                        </a>
                    </li>
                </ul>

            </div>
        </div>

    )
}

export default SideBarManagerSection