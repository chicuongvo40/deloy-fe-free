import React from 'react'
import './body.css'
import { Menu, MenuItem, IconButton } from '@mui/material';
import LogoutIcon from '@mui/icons-material/Logout';
import { useEffect, useMemo, useState, useRef, useCallback } from 'react';
import { BiSearchAlt } from 'react-icons/bi'
import { TbMessageCircle } from 'react-icons/tb'
import { MdOutlineNotificationsNone } from 'react-icons/md'
import { BsArrowRightShort } from 'react-icons/bs'
import { useAuth } from '../../context/AuthContext';
import { useNavigate } from 'react-router-dom';
import img from '../../assets/profile.jpeg'
import img2 from '../../assets/lg.png'
import video from '../../assets/fish.mp4'

const BodyManager = () => {
  const [anchorEl, setAnchorEl] = useState(null);
  const { user, removeUser, removeToken } = useAuth();
  const handleMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };
  const navigate = useNavigate();
  const handleLogout = () => {
    removeUser();
    removeToken();
    navigate('/login');
  };
  return (
    <div className='topSection1'>
      <div className="headerSection flex">
        <div className="title">
          <h1>Welcom to Amazing Tech</h1>
          <p>Hello Cuong, Welcome Back!!!</p>
        </div>

        <div className="searchBar flex">
          <input type='text' placeholder='Search' />
          <BiSearchAlt className='icon' />
        </div>

        <div className="adminDiv flex">
          <TbMessageCircle className='icon' />
          <MdOutlineNotificationsNone className='icon' />
          <div className='adminImage'>
            <img src={img} alt='Admin Image' onClick={handleMenuOpen}/>
          </div>
          <Menu
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={handleMenuClose}
      >
        <MenuItem onClick={handleLogout}>
          <LogoutIcon sx={{ marginRight: 1 }} />
          Logout
        </MenuItem>
      </Menu>
        </div>
      </div>

      <div className='cardSection flex'>
        
        <div className="rightCard flex">
          <div className="videoDiv">
            <video src={video} autoPlay loop muted></video>
          </div>
        </div>

        <div className="leftCard flex">
          <div className="main flex">

            <div className="textDiv">
              <h1>My Stat</h1>
            </div>
            <div className="imgDiv">
              <img src={img2} alt='Image Name' />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default BodyManager