// Styles
import styles from './EditCampaignAdm.module.css';
// Icons
import { AiOutlineClose } from 'react-icons/ai';
// Components
import { Input } from '../../Input';
import { Button } from '../../Button';
import { useQuery, useMutation } from '@tanstack/react-query';
// Modules
import { motion } from 'framer-motion';
import { useAxios } from '../../../context/AxiosContex';
//functions
import { useState } from 'react';
import { useEffect } from 'react';
import { toast } from 'react-hot-toast';
import { useRef } from 'react';
export function EditCampaignAdm({ idBrand, setEditModal, refetch}) {
  const { getLevelById, updateSource } = useAxios();
  const ods = useRef();
  const [branchName, setBrandName] = useState('');

  const {
    data: getBrandId,
  } = useQuery(
    {
      queryKey: ['getbrandbyid'],
      queryFn: async () => await getLevelById(idBrand),
    }
  );

  useEffect(() => {
    if (getBrandId) {
      setBrandName(getBrandId.levelName);
    }
  }, [getBrandId]);

  //* update brand mutations
  const updateBrandMutation = useMutation({
    mutationFn: (data) => {
      return updateSource(data).then((res) => {
        refetch();
        return res;
      });
    },
    onSuccess: (data) => { 
      console.log(data);
      if (data?.status === 'Success') {
        toast('Update thành công', {
          icon: '👏',
          style: {
            borderRadius: '10px',
            background: '#333',
            color: '#fff',
          },
        });
        setEditModal(false);
      }
    },
  });
  function handleUpdateBrand(e) {
    e.preventDefault();
    const dataPatch = {
      id: idBrand,
      sourceName: branchName,
    };
    if (
    
      !branchName 
    ) {
      toast('Không được bỏ trống mục nào', {
        icon: '👏',
        style: {
          borderRadius: '10px',
          background: '#333',
          color: '#fff',
        },
      });
      return;
    }
    updateBrandMutation.mutate({ data: dataPatch });
  }
  return (
    <motion.div
      initial={{ opacity: 0, transition: 0.5 }}
      animate={{ opacity: 1, transition: 0.5 }}
      transition={{ type: 'spring' }}
      className={styles.formmoal}
    >
      <form>
        <div className={styles.head}>
          <h6>SỬA KÊNH KHÁCH HÀNG</h6>
          <AiOutlineClose
            onClick={() => setEditModal(false)}
            style={{ color: 'grey', fontSize: '20px', cursor: 'pointer' }}
          />
        </div>
        <div className={styles.fields}>
        <div className={styles.field}>
            <label>Tên kênh khách hàng</label>
            <Input
              value={branchName}
              onChange={(e) => setBrandName(e.target.value)}
              type='text'
            />
          </div>
         
          <div className={styles.buttons}>
            <Button
              className={styles.cancelbtn}
              onClick={(e) => {
                e.preventDefault();
                setEditModal(false);
              }}
            >
              HỦY
            </Button>
            <Button
              onClick={handleUpdateBrand}
              className={styles.addbtn}
            >
              SỬA KÊNH KH
            </Button>
          </div>
        </div>
      </form>
    </motion.div>
  );
}
