// Styles
import styles from './AddCampaignAdm.module.css';
// Icons
import { AiOutlineClose } from 'react-icons/ai';
// Components
import { Input } from '../../Input';
import { Button } from '../../Button';
// Context
import { useAxios } from '../../../context/AxiosContex';
// Modules
import { motion } from 'framer-motion';
import { useMutation } from '@tanstack/react-query';
import { toast } from 'react-hot-toast';
import { useState } from 'react';
import { useRef } from 'react';
export function AddCampaignAdm({ setOpenModal, refetch }) {
  const [branchName, setBrandName] = useState('');
  const { postCustomerLevel } = useAxios();
  const postCustomerMutation = useMutation({
    mutationFn: (data) => {
      return postCustomerLevel(data).then((res) => {
        return res;
      });
    },
    onSuccess(data) {
      console.log(data);
      if (data?.status !== 400 ) {
        refetch();
        toast('Tạo thành công', {
          icon: '👏',
          style: {
            borderRadius: '10px',
            background: '#333',
            color: '#fff',
          },
        });
        setOpenModal(false);
      } else {
        toast('Có lỗi trong quá trình tạo CustomerLevel', {
          icon: '👏',
          style: {
            borderRadius: '10px',
            background: '#333',
            color: '#fff',
          },
        });
      }
    },
  });
  //* Hàm tạo khách mới
  const handleAddBrand = (e) => {
    e.preventDefault();
    if (
      !branchName 
    ) {
      toast('Không được bỏ trống', {
        icon: '👏',
        style: {
          borderRadius: '10px',
          background: '#333',
          color: '#fff',
        },
      });
      return;
    }
    const data = {
      levelName: branchName,
    };
    postCustomerMutation.mutate({
      data
    });
  };
  return (
    <motion.div
      initial={{ opacity: 0, transition: 0.5 }}
      animate={{ opacity: 1, transition: 0.5 }}
      transition={{ type: 'spring' }}
      className={styles.formmoal}
    >
      <form>
        <div className={styles.head}>
          <h6> THÊM LEVEL KHÁCH HÀNG</h6>
          <AiOutlineClose
            onClick={() => setOpenModal(false)}
            style={{ color: 'grey', fontSize: '20px', cursor: 'pointer' }}
          />
        </div>
        <div className={styles.fields}>
        
          <div className={styles.field}>
            <label>Tên Cấp Độ</label>
            <Input
              value={branchName}
              onChange={(e) => setBrandName(e.target.value)}
              type='text'
            />
          </div>
         
          <div className={styles.buttons}>
            <Button
              className={styles.cancelbtn}
              onClick={(e) => {
                e.preventDefault();
                setOpenModal(false);
              }}
            >
              Hủy
            </Button>
            <Button
              onClick={handleAddBrand}
              className={styles.addbtn}
            >
              THÊM LEVEL KHÁCH HÀNG
            </Button>
          </div>
        </div>
      </form>
    </motion.div>
  );
}
