// Styles
import styles from './AddCampaignAdm.module.css';
// Icons
import { AiOutlineClose } from 'react-icons/ai';
// Components
import { Input } from '../../../components/Input';
import { Button } from '../../../components/Button';
// Context
import { useAxios } from '../../../context/AxiosContex';
// Modules
import { motion } from 'framer-motion';
import { useMutation } from '@tanstack/react-query';
import { toast } from 'react-hot-toast';
import { useState } from 'react';
import { useRef } from 'react';
export function AddCampaignAdm({ setOpenModal, refetch }) {
  const ods = useRef();
  const [address, setAddress] = useState('');
  const [branchName, setBrandName] = useState('');
  const { postBrand } = useAxios();
  const postCustomerMutation = useMutation({
    mutationFn: (data) => {
      return postBrand(data).then((res) => {
        return res;
      });
    },
    onSuccess(data) {
      console.log(data);
      if (data?.status !== 400 ) {
        refetch();
        toast('Tạo thành công', {
          icon: '👏',
          style: {
            borderRadius: '10px',
            background: '#333',
            color: '#fff',
          },
        });
        setOpenModal(false);
      } else {
        toast('Có lỗi trong quá trình tạo Brand', {
          icon: '👏',
          style: {
            borderRadius: '10px',
            background: '#333',
            color: '#fff',
          },
        });
      }
    },
  });
  //* Hàm tạo khách mới
  const handleAddBrand = (e) => {
    e.preventDefault();
    if (
      !address ||
      !branchName 
    ) {
      toast('Không được bỏ trống', {
        icon: '👏',
        style: {
          borderRadius: '10px',
          background: '#333',
          color: '#fff',
        },
      });
      return;
    }
    const data = {
      odsId: ods.current?.value,
      address: address,
      branchName: branchName,
    };
    postCustomerMutation.mutate({
      data
    });
  };
  return (
    <motion.div
      initial={{ opacity: 0, transition: 0.5 }}
      animate={{ opacity: 1, transition: 0.5 }}
      transition={{ type: 'spring' }}
      className={styles.formmoal}
    >
      <form>
        <div className={styles.head}>
          <h6>THÊM CHI NHÁNH</h6>
          <AiOutlineClose
            onClick={() => setOpenModal(false)}
            style={{ color: 'grey', fontSize: '20px', cursor: 'pointer' }}
          />
        </div>
        <div className={styles.fields}>
        
          <div className={styles.field}>
            <label>Tên Chi Nhánh</label>
            <Input
              value={branchName}
              onChange={(e) => setBrandName(e.target.value)}
              type='text'
            />
          </div>
         
          <div className={styles.field}>
            <label>Địa Chỉ</label>
            <Input
              value={address}
              onChange={(e) => setAddress(e.target.value)}
              type='text'
            />
          </div>
          <div className={styles.field}>
              <label>ODS</label>
              <select ref={ods}>
                <option value='1'>Nhánh 1</option>
                <option value='2'>Nhánh 2</option>
              </select>
            </div>
          <div className={styles.buttons}>
            <Button
              className={styles.cancelbtn}
              onClick={(e) => {
                e.preventDefault();
                setOpenModal(false);
              }}
            >
              HỦY
            </Button>
            <Button
              onClick={handleAddBrand}
              className={styles.addbtn}
            >
              THÊM CHI NHÁNH
            </Button>
          </div>
        </div>
      </form>
    </motion.div>
  );
}
