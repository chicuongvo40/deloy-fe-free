import { Box, Card, Grid, IconButton, styled, Tooltip,Typography } from "@mui/material";
import { AttachMoney, Group, ShoppingCart, Store, ArrowRightAlt } from "@mui/icons-material";

// STYLED COMPONENTS
const StyledCard = styled(Card)(({ theme }) => ({
  display: "flex",
  flexWrap: "wrap",
  alignItems: "center",
  justifyContent: "space-between",
  padding: "4px !important",
  background: theme.palette.background.paper,
  [theme.breakpoints.down("sm")]: { padding: "4px !important" }
}));

const ContentBox = styled(Box)(({ theme }) => ({
  display: "flex",
  flexWrap: "wrap",
  alignItems: "center",
  "& small": { color: theme.palette.text.secondary },
  "& .icon": { opacity: 0.6, fontSize: "44px", color: theme.palette.primary.main }
}));

const Heading = styled("h6")(({ theme }) => ({
  margin: 0,
  marginTop: "4px",
  fontSize: "14px",
  fontWeight: "500",
  color: theme.palette.primary.main
}));

export default function StatCards() {
  const cardList = [
    { name: "Tiếp Nhận", color: "lightgreen" },
    { name: "Đang Xử Lý", color: "lightpink" },
    { name: "Đã Xử Lý", color: "lightyellow" },
    { name: "Kết thúc", color: "lightblue" }
  ];

  return (
    <Grid container spacing={3} sx={{ mb: "24px" }}>
      {cardList.map(({ name, color }) => (
        <Grid item xs={12} md={3} key={name}>
          <StyledCard elevation={6} style={{ backgroundColor: color,display: 'flex', alignItems: 'center', justifyContent: 'center'  }}>
            <ContentBox>
              <Box ml="12px">
                <Typography sx={{fontSize:17}}>{name}</Typography>
              </Box>
            </ContentBox>
          </StyledCard>
        </Grid>
      ))}
    </Grid>
  );
}
