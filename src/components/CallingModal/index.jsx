// Styles
import styles from './FormModal.module.css';
// Icons
import { AiOutlineClose } from 'react-icons/ai';
// Context
import { useAuth } from '../../context/AuthContext';
import { useAxios } from '../../context/AxiosContex';
// Modules
import { motion } from 'framer-motion';
import { useMutation, useQuery } from '@tanstack/react-query';
import { toast } from 'react-hot-toast';
import { useRef } from 'react';
import { format } from 'date-fns';
import { useState } from 'react';
import PhoneIcon from '@mui/icons-material/Phone';
import CallEndIcon from '@mui/icons-material/CallEnd';
import {
  Avatar,
  Box,
  Card,
  CardActions,
  CardContent,
  Collapse,
  Button,
  Typography,
  Rating,
  useTheme,
  useMediaQuery,
  CardMedia,
  Paper
} from "@mui/material";
export function FormModal({ handleOnClick,setOffStatus,phone }) {

  return (
    <motion.div
      initial={{ opacity: 0, transition: 0.5 }}
      animate={{ opacity: 1, transition: 0.5 }}
      transition={{ type: 'spring' }}
      className={styles.formmoal}
    >
      <form className={styles.form1}>
        <Box display="block" alignItems="center" justifyContent="center">
      <Box display="flex" alignItems="center" justifyContent="center">
      <Avatar alt="Avatar" src="/path/to/image.jpg" />
      </Box>
      <Box display="flex" alignItems="center" justifyContent="center" >     
            <Box marginLeft={2}>
                <p>Có Cuộc Gọi từ {phone}</p>
                <Button variant="contained" color="primary" onClick={handleOnClick}> 
                    Chấp Nhận
                </Button>
                <Button variant="contained" color="error" sx={{ml:2}} onClick={setOffStatus}>
                    Từ Chối
                </Button>
            </Box>
        </Box>
        </Box>
      </form>
    </motion.div>
  );
}
