// Styles
import styles from './FormModal.module.css';
// Icons
import { AiOutlineClose } from 'react-icons/ai';
// Components
import { Input } from '../Input';
import { Button } from '../Button';
// Context
import { useAuth } from '../../context/AuthContext';
import { useAxios } from '../../context/AxiosContex';
// Modules
import { motion } from 'framer-motion';
import { useMutation, useQuery } from '@tanstack/react-query';
import { toast } from 'react-hot-toast';
import { useRef } from 'react';
import { format } from 'date-fns';
export function FormModal({ setOpenModal, refetch }) {
  //* Link api
  const { postCustomer,getSource,getCustomerLevel } =
    useAxios();

  //* Ref data
  const src = useRef();
  const customerLevelId = useRef();
  const lastName = useRef();
  const firstName = useRef();
  const phoneNumber = useRef();
  const address = useRef();
  const dayOfBirth = useRef();
  const name = useRef();
  const gender = useRef();

  //* Mutatuion tạo khách hàng
  const postCustomerMutation = useMutation({
    mutationFn: (data) => {
      return postCustomer(data).then((res) => {
        return res;
      });
    },
    onSuccess(data) {
      console.log(data);
      if (data?.status !== 400 ) {
        refetch();
        toast('Tạo thành công', {
          icon: '👏',
          style: {
            borderRadius: '10px',
            background: '#333',
            color: '#fff',
          },
        });
        setOpenModal(false);
      } else {
        toast('Có lỗi trong quá trình tạo user', {
          icon: '👏',
          style: {
            borderRadius: '10px',
            background: '#333',
            color: '#fff',
          },
        });
      }
    },
  });

  const {
    data: source,
  } = useQuery(
    {
      queryKey: ['source'],
      queryFn: async () => await getSource(),
    }
  );

  const {
    data: level,
  } = useQuery(
    {
      queryKey: ['level'],
      queryFn: async () => await getCustomerLevel(),
    }
  );

  //* Hàm tạo khách mới
  const handleAddUser = (e) => {
    e.preventDefault();
    if (
      !customerLevelId.current.value ||
      !lastName.current.value ||
      !firstName.current.value ||
      !phoneNumber.current.value ||
      !address.current.value ||
      !dayOfBirth.current.value ||
      !name.current.value ||
      !gender.current.value 
    ) {
      toast('Không được bỏ trống', {
        icon: '👏',
        style: {
          borderRadius: '10px',
          background: '#333',
          color: '#fff',
        },
      });
      return;
    }
    const data = {
      customerLevelId: customerLevelId.current?.value,
      sourceId: src.current?.value,
      branchId: 1,
      lastName: lastName.current?.value,
      firstName: firstName.current?.value,
      phoneNumber: phoneNumber.current?.value,
      status: 1,
      gender: gender.current?.value,
      address: address.current?.value,
      dayOfBirth: dayOfBirth.current?.value,
      dateCreated:  format(Date.now(), 'yyyy-MM-dd'),
      lastEditedTime: format(Date.now(), 'yyyy-MM-dd'),
      name: name.current?.value
    };
    postCustomerMutation.mutate({
      data
    });
  };

  return (
    <motion.div
      initial={{ opacity: 0, transition: 0.5 }}
      animate={{ opacity: 1, transition: 0.5 }}
      transition={{ type: 'spring' }}
      className={styles.formmoal}
    >
      <form>
        <div className={styles.head}>
          <h6>Thêm khách hàng</h6>
          <AiOutlineClose
            onClick={() => setOpenModal(false)}
            style={{ color: 'grey', fontSize: '20px', cursor: 'pointer' }}
          />
        </div>
        <div className={styles.fields}>
          <div className={styles.field}>
            <label>First Name</label>
            <Input
              ref={firstName }
              type='text'
            />
          </div>
          <div className={styles.field}>
            <label>Last Name</label>
            <Input
              ref={lastName }
              type='text'
            />
          </div>
          <div className={styles.field}>
            <label>Name</label>
            <Input
              ref={name }
              type='text'
            />
          </div>
          <div className={styles.field}>
            <label>Contract Code</label>
            <Input
              
              type='text'
            />
          </div>
          <div className={styles.field}>
            <label>Phone Number</label>
            <Input
              ref={phoneNumber}
              type='text'
            />
          </div>
          <div className={styles.field}>
            <label>Address</label>
            <Input
              ref={address}
              type='text'
            />
          </div>
          <div className={styles.field}>
            <label>Email</label>
            <Input
              type='text'
            />
          </div>
          <div className={styles.flex}>
            <div className={styles.field}>
              <label>Giới tính</label>
              <select ref={gender}>
                <option value='1'>Nam</option>
                <option value='2'>Nữ</option>
              </select>
            </div>
            <div className={styles.flex}>

            <div className={styles.field}>
            <label>Source</label>
            <select ref={src}>
              {source &&
                source.map((lv, index) => {
                  if (lv?.sourceName) {
                    return (
                      <option
                        key={index}
                        value={lv.id}
                      >
                        {lv.sourceName}
                      </option>
                    );
                  }
                })}
            </select>
          </div>

          
          
            <div className={styles.field}>
              <label>Birthday</label>
              <Input
                ref={dayOfBirth}
                type='date'
              />
            </div>
          </div>
          <div className={styles.field}>
            <label>Level</label>
            <select ref={customerLevelId }>
            {level &&
                level.map((lv, index) => {
                  if (lv?.levelName) {
                    return (
                      <option
                        key={index}
                        value={lv.id}
                      >
                        {lv.levelName}
                      </option>
                    );
                  }
                })}
            </select>
          </div>
         
          <div className={styles.buttons}>
            <Button
              className={styles.cancelbtn}
              onClick={(e) => {
                e.preventDefault();
                setOpenModal(false);
              }}
            >
              Hủy
            </Button>
            <Button
               onClick={handleAddUser}
              className={styles.addbtn}
            >
              Thêm khách hàng
            </Button>
          </div>
        </div>
        </div>
      </form>
    </motion.div>
  );
}
