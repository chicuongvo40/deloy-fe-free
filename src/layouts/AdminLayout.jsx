import React from "react";
import { Navigate,Outlet } from "react-router-dom";
import './AdminLayout.css';
import BodyManager from "../components/BodyAdmin/Body";
import SideBar from '../components/SideBarAdminSection/SideBar'
import { useAuth } from '../context/AuthContext';
export function AdminLayout() {
  const { user} = useAuth();;
  return (
    <div className="container">
      <SideBar />
      <div className="mainContent">
        <BodyManager />
        <div className="mainContent1">
        <div className="bottom flex">
          <Outlet />
        </div>
        </div>
      </div>
    </div>
  );
}
